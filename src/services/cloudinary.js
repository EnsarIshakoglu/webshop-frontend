import axios from 'axios';

const cloud_name= "degktxldj";
const upload_preset= "tnuwa2st";
const config = {
    headers: { "X-Requested-With": "XMLHttpRequest" }
};

export const uploadImage = (image) => {
    let url = `https://api.cloudinary.com/v1_1/${cloud_name}/upload`;
    let formData = new FormData();
    formData.append("upload_preset", upload_preset);
    formData.append("file", image);

    return axios.post(url, formData, config)
};

export const getImageUnavailable = () =>{
    return 'holcom/vr0f0ikhicvxfwdniism.png'
};