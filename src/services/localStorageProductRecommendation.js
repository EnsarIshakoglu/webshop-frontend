export const addVisitedCategory = (categoryString) =>{
    let cart = getLocalStorageArray();
    if(cart.indexOf(categoryString) > -1) return;
    if(cart.length < 3) cart.unshift(categoryString);
    else {
        cart.unshift(categoryString);
        cart.pop();
    }

    setLocalStorageArray(cart);
};

export const getVisitedCategories = () =>{
    return getLocalStorageArray();
};

export let getLocalStorageArray = () =>{
    return JSON.parse(localStorage.getItem('visitedCategories')) || [];
};

export let setLocalStorageArray = (arr) => {
    localStorage.setItem('visitedCategories', JSON.stringify(arr));
};