export const addCartItem = (cartItem) =>{
    let cart = JSON.parse(localStorage.getItem('cart')) || [];
    cart.push(cartItem);

    localStorage.setItem('cart', JSON.stringify(cart));
};

export const deleteCartItem = (cartItem) =>{
    let cart = JSON.parse(localStorage.getItem('cart')) || [];

    let index = 0;

    for(let x = 0; x < cart.length; x++){
        if(cart[x].product.uuId === cartItem.product.uuId){
            index = x;
            break;
        }
    }

    cart.splice(index, 1);

    localStorage.setItem('cart', JSON.stringify(cart));
};

export const editQuantity = (cartItem) =>{
    let cart = JSON.parse(localStorage.getItem('cart')) || [];
    let index = 0;

    for(let x = 0; x < cart.length; x++){
        if(cart[x].product.uuId === cartItem.product.uuId){
            index = x;
            break;
        }
    }

    cart[index] = cartItem;

    localStorage.setItem('cart', JSON.stringify(cart));
};