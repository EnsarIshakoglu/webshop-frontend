import { ToastProgrammatic as toast } from 'buefy'
import { DialogProgrammatic as dialog} from "buefy";

export const showErrorToaster = (errorMessage, position) =>{
    toast.open({
        message: errorMessage,
        type: "is-danger",
        position: position
    })
};

export const showSuccessToaster = (successMessage, position) =>{
    toast.open({
        message: successMessage,
        type: "is-success",
        position: position
    })
};

export const showDialog = (title, message, confirmText, type, hasIcon) => {
    dialog.alert({
        title: title,
        message: message,
        confirmText: confirmText,
        type: type,
        hasIcon: hasIcon
    })
};

export const showDialogWithCallBack = (title, message, confirmText, type, hasIcon, callBack) => {
    dialog.alert({
        title: title,
        message: message,
        confirmText: confirmText,
        type: type,
        hasIcon: hasIcon,
        onConfirm: callBack
    })
};
