import axios from 'axios';

const baseUrl =  process.env.NODE_ENV === 'production'
    ? 'https://backend-webshop.herokuapp.com'
    : 'http://192.168.0.108:8082' ;

export const getCategories = () => {
    return axios.get(baseUrl +'/categories').then(response => {
        return response;
    });
};

export const getAdminAccess = () => {
    return axios.get(baseUrl +'/checkAdminAccess', {
        withCredentials: true
    }).then((response) => {
        return response;
    }).catch((response) => {
        return response;
    });
};

export const getShoppingCart = () => {
    return axios.get(baseUrl +'/cart', {
        withCredentials: true
    }).then((response) => {
        return response;
    });
};

export const getOrderHistory = () => {
    return axios.get(baseUrl +'/orders', {
        withCredentials: true
    }).then((response) => {
        return response;
    });
};

export const getUserData = () => {
    return axios.get(baseUrl +'/users/me', {
        withCredentials: true
    }).then((response) => {
        return response;
    }).catch((response) => {
        return response;
    });
};

export const updateUserData = (userData) => {
    return axios.put(baseUrl +'/users', userData, {
        withCredentials: true
    }).then((response) => {
        return response;
    }).catch((response) => {
        return response;
    });
};

export const getProducts = (pageNumber, categoryName, searchTerm, size) => {
    let apiUrl = `/products?`;
    if(pageNumber) apiUrl += `pageNr=${pageNumber}`;
    if(searchTerm) apiUrl += `&title=${searchTerm}`;
    if(categoryName) apiUrl += `&categories=${categoryName}`;
    if(size) apiUrl += `&pageSize=${size}`;

    return axios.get(baseUrl + apiUrl).then(response => {
        return response;
    });
};

export const getProductByUuId = (uuId) => {
    return axios.get(baseUrl + `/products/${uuId}`).then(response => {
        return response;
    });
};

export const registerUser = (userData) => {
    return axios.post(baseUrl + '/users', JSON.stringify(userData), {
        headers: {
            "Content-Type": "application/json"
        }
    });
};

export const login = (userData) => {
    return axios.post(baseUrl + '/login', JSON.stringify(userData), {
        withCredentials: true
    });
};

export const logout = () => {
    return axios.post(baseUrl + '/logout', null, {
        withCredentials: true
    });
};

export const updateProduct = (product) => {
    return axios.put(baseUrl + '/products', product, {
        withCredentials: true
    });
};

export const updateCartItem = (cartItem) => {
    return axios.put(baseUrl + '/cart', cartItem, {
        withCredentials: true
    });
};

export const addProduct = (product) => {
    return axios.post(baseUrl + '/products', product, {
        withCredentials: true
    });
};

export const storeImage = (storableImage) => {
    return axios.post(baseUrl + '/products/store-image', storableImage, {
        withCredentials: true
    });
};

export const addCartItem = (cartItem) => {
    return axios.post(baseUrl + '/cart', cartItem, {
        withCredentials: true
    });
};

export const deleteProduct = (product) => {
    return axios.delete(baseUrl + '/products', {
        data: product,
        withCredentials: true
    });
};

export const deleteCartItem = (cartItem) => {
    return axios.delete(baseUrl + '/cart', {
        data: cartItem,
        withCredentials: true
    });
};

export const makeOrder = (order) => {
    return axios.post(baseUrl + '/orders', order, {
        withCredentials: true
    });
};