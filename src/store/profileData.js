export default {
    state:{
        profileData: null,
        loggedIn: false,
        timesDataFetched: 0
    },
    mutations:{
        SET_PROFILE_DATA(state, profileData){
            state.profileData = profileData;
        },
        SET_LOGGED_IN(state, loggedIn){
          state.loggedIn = loggedIn;
        },
        CLEAR_PROFILE_DATA(state){
            state.profileData = null;
        },
        INCREMENT_TIMES_DATA_FETCHED(state){
            state.timesDataFetched++;
        }
    },
    actions:{
        setProfileData(context, profileData){
            context.commit('SET_PROFILE_DATA', profileData);
        },
        setLoggedIn(context, loggedIn){
            context.commit('SET_LOGGED_IN', loggedIn);
        },
        clearProfileData(context){
            context.commit('CLEAR_PROFILE_DATA');
        },
        incrementTimesDataFetched(context){
            context.commit('INCREMENT_TIMES_DATA_FETCHED');
        }
    },
    getters:{
        getProfileData: state => state.profileData,
        getLoggedInStatus: state => state.loggedIn,
        getTimesFetched: state => state.timesDataFetched
    }
}