export default {
    state:{
        products: null,
        totalPrice: 0
    },
    mutations:{
        SET_SHOPPING_CART(state, products){
          state.products = products;
        },
        ADD_PRODUCT_TO_SHOPPING_CART(state, cartItem){
            if(!state.products) state.products = [];

            state.products.push(cartItem);
            state.totalPrice += cartItem.product.price;
        },
        REMOVE_PRODUCT_FROM_SHOPPING_CART(state, cartItem){
            let products = state.products;

            for(let x = 0; x < products.length; x++){
                if(products[x].product.uuId === cartItem.product.uuId) {
                    products.splice(x, 1);
                    state.totalPrice -= cartItem.product.price * cartItem.quantity;
                    return
                }
            }
        },
        CALCULATE_SHOPPING_CART_TOTAL_PRICE(state){
            state.totalPrice = 0;
            if(!state.products) return;
            state.products.forEach(p => state.totalPrice += p.product.price * p.quantity);

            state.totalPrice = Math.round((state.totalPrice + Number.EPSILON) * 100) / 100
        }
    },
    getters:{
        getShoppingCartProducts: state => state.products,
        getShoppingCartLength: state => state.products.length,
        getShoppingCartTotalPrice: state => state.totalPrice
    },
    actions:{
        setShoppingCart(context, products) {
            context.commit('SET_SHOPPING_CART', products);
            context.commit('CALCULATE_SHOPPING_CART_TOTAL_PRICE');
        },
        addProductToShoppingCart(context, cartItem){
            context.commit('ADD_PRODUCT_TO_SHOPPING_CART', cartItem);
            context.commit('CALCULATE_SHOPPING_CART_TOTAL_PRICE');
        },
        removeProductFromShoppingCart(context, cartItem){
            context.commit('REMOVE_PRODUCT_FROM_SHOPPING_CART', cartItem);
            context.commit('CALCULATE_SHOPPING_CART_TOTAL_PRICE');
        },
        calculateShoppingCartTotalPrice(context){
            context.commit('CALCULATE_SHOPPING_CART_TOTAL_PRICE');
        }
    }
}

/*
    mockProducts:
    [
            {
                title: 'test',
                price: 200,
                onSale: true,
                rating: 4.8,
                description: 'Cool productCool productCool productCool productCool productCool productCool productCool productCool productCool productCool productCool productCool productCool product',
                quantity: 1
            },
            {
                title: 'test',
                price: 200,
                onSale: true,
                rating: 4.8,
                description: 'Cool productCool productCool productCool productCool productCool productCool productCool productCool productCool productCool productCool productCool productCool product',
                quantity: 1
            },
            {
                title: 'test',
                price: 200,
                onSale: true,
                rating: 4.8,
                description: 'Cool productCool productCool productCool productCool productCool productCool productCool productCool productCool productCool productCool productCool productCool product',
                quantity: 1
            },
            {
                title: 'test',
                price: 200,
                onSale: true,
                rating: 4.8,
                description: 'Cool productCool productCool productCool productCool productCool productCool productCool productCool productCool productCool productCool productCool productCool product',
                quantity: 1
            }
        ]
 */