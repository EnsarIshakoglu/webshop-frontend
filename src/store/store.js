import Vue from 'vue'
import Vuex from 'vuex'
import shoppingCart from "./shoppingCart";
import userData from "./profileData";

Vue.use(Vuex);

export default new Vuex.Store({
    modules:{
        shoppingCart: shoppingCart,
        userData: userData
    },
    state:{

    },
    mutations:{

    },
    getters:{

    },
    actions:{

    }
})
