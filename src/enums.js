export default class enums {
    static modals = {
        REGISTER: 'loginModal',
        LOGIN: 'registerModal',
        SHOPPING_CART: 'shoppingCart'
    };

    static apiStatus = {
        OK: 200,
        BAD_REQUEST: 400,
        UNAUTHORIZED: 401,
        NOT_FOUND: 404
    }
}