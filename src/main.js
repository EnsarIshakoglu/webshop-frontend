import Vue from 'vue'
import App from './App.vue'
import Buefy from 'buefy'
import store from "./store/store";
import 'buefy/dist/buefy.css'
import router from './router'
import * as apiService from './services/apiservice'
import * as helpers from './services/helpers'
import * as localStorageCart from './services/localStorageCart'
import * as localStorageProductRecommendation from './services/localStorageProductRecommendation'
import * as cloudinary from './services/cloudinary'
import axios from "axios";
import vueDebounce from 'vue-debounce'
import Cloudinary from 'cloudinary-vue';

Vue.use(Cloudinary, {
  configuration: {
    cloudName: "degktxldj",
    api_key: "928885995393385",
    api_secret: "0r7YbQWATt8h3IBlAQMhWN670YE"
  }
});

Vue.use(Buefy);
Vue.use(vueDebounce);
Vue.config.productionTip = false;
Vue.prototype.$apiService = apiService;
Vue.prototype.$cloudinary = cloudinary;
Vue.prototype.$helpers = helpers;
Vue.prototype.$localStorageCart = localStorageCart;
Vue.prototype.$localStorageProductRecommendation = localStorageProductRecommendation;

Vue.prototype.$axios = axios.create({
  baseURL: `http://localhost:8082/`,
  headers: {
    'Access-Control-Allow-Origin': 'http://localhost:8080'
  }
});

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app');
