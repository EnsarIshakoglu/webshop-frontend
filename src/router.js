import Vue from 'vue'
import Router from 'vue-router'
import About from './components/About.vue';
import Home from './components/Home.vue';
import ProductInfo from './components/ProductInfo.vue';
import SearchResult from "./components/SearchResult";
import AdminPage from "./components/AdminPage";
import ProfilePage from "./components/ProfilePage";
import EditProfile from "./components/profile_views/EditProfile";
import OrderHistory from "./components/profile_views/OrderHistory";

Vue.use(Router);

export default new Router({
   mode: 'history',
   base: '/webshop-frontend/',
   hash: false,
   routes: [
    {
        path: '/about',
        name: 'about',
        component: About
    },
    {
        path: '/',
        name: 'home',
        component: Home
    },
    {
        path: '/login',
        name: 'login',
        component: Home,
        props: {loginModalIsActive: true}
    },
    {
        path: '/register',
        name: 'register',
        component: Home,
        props: {registerModalIsActive: true}
    },
    {
        path: '/p',
        name: 'productInfo',
        component: ProductInfo,
        props: true
    },
    {
        path: '/s',
        name: 'categoryItems',
        component: SearchResult,
        props: true
    },
    {
        path: '/admin',
        name: 'admin',
        component: AdminPage
    },
    {
        path: '/profile',
        name: 'profile',
        component: ProfilePage,
        children: [
            {
                path: '',
                component: EditProfile
            },
            {
                path: 'order-history',
                component: OrderHistory
            }
        ]
    }
  ]
})