const CompressionWebpackPlugin = require("compression-webpack-plugin");

module.exports = {
    publicPath: process.env.NODE_ENV === 'production'
        ? '/webshop-frontend/'
        : '/',
    chainWebpack(config) {
        config.plugins.delete('prefetch');
        config.plugin('CompressionPlugin').use(CompressionWebpackPlugin);
    },
    pluginOptions: {
        webpackBundleAnalyzer: {
            openAnalyzer: false
        }
    }
};