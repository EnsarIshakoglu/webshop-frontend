import buefy from 'buefy';
import {createLocalVue} from "@vue/test-utils";

const localVue = createLocalVue();
localVue.use(buefy);

export default {
    localVue
}