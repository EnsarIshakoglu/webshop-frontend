import { shallowMount } from '@vue/test-utils'
import config from "../test_config.js";
import ProductEditorInput from '@/components/admin_page/ProductEditorInput.vue'

const localVue = config.localVue;
let wrapper;
let label = "TEST";
let placeholder = "This is a placeholder";

describe('ProductEditorInput', () => {
  beforeEach(() => {
    wrapper = shallowMount(ProductEditorInput, {
      localVue,
      propsData:{
        label: label,
        placeholder: placeholder
      }
    });
  });

  it('exists', () => {
    expect(wrapper.exists()).toBeTruthy();
  });

  it('is a vue component', () => {
    expect(wrapper.isVueInstance()).toBeTruthy();
  });

  it('contains a label with text specified in props', () => {
    expect(wrapper.find('.padding-bottom').props().label).toBe(label);
  });

  it('placeholder is equal to value specified in property', () => {
    expect(wrapper.find('.padding-bottom').props().placeholder).toEqual(placeholder);
  });

  it('label is rendered', () => {
    expect(wrapper.html()).toContain(label);
  });

  it('placeholder is rendered', () => {
    expect(wrapper.html()).toContain(placeholder);
  });

  it('default type prop is text', () => {
    expect(wrapper.find('.padding-bottom').props().type).toEqual('text');
  });

  it('default min prop is 1', () => {
    expect(wrapper.find('.padding-bottom').props().min).toEqual(1);
  });
});
