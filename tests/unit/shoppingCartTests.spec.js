import * as localStorageCart from "../../src/services/localStorageCart.js";
import LocalStorageMock from "../LocalStorageMock";

global.localStorage = new LocalStorageMock();

let cartItem = {
    product: {
        title: 'test-product',
        price: 500,
        uuId: 1
    },
    quantity: 1,
    boughtPrice: 500
};

let cartItem2 = {
    product: {
        title: 'test-product-2',
        price: 800,
        uuId: 2
    },
    quantity: 1,
    boughtPrice: 800
};

describe('Shopping cart storage', () => {
    beforeEach(() => {
        localStorage.clear();
    });

    it('can add one product to localStorage cart', () => {
        localStorageCart.addCartItem(cartItem);
        let expectedResult = JSON.stringify([cartItem]);

        expect(localStorage.getItem('cart')).toBe(expectedResult);
    });

    it('can add multiple products to localStorage cart', () => {
        localStorageCart.addCartItem(cartItem);
        localStorageCart.addCartItem(cartItem2);
        let expectedResult = JSON.stringify([cartItem, cartItem2]);

        expect(localStorage.getItem('cart')).toBe(expectedResult);
    });

    it('can remove product from localStorage cart', () => {
        localStorageCart.addCartItem(cartItem);
        localStorageCart.deleteCartItem(cartItem);

        let expectedResult = JSON.stringify([]);

        expect(localStorage.getItem('cart')).toBe(expectedResult);
    });

    it('can remove product from localStorage cart when multiple products added', () => {
        localStorageCart.addCartItem(cartItem);
        localStorageCart.addCartItem(cartItem2);

        localStorageCart.deleteCartItem(cartItem);

        let expectedResult = JSON.stringify([cartItem2]);

        expect(localStorage.getItem('cart')).toBe(expectedResult);
    });

    it('can edit product quantity', () => {
        localStorageCart.addCartItem(cartItem);
        let newCartItem = {...cartItem, ...{quantity: 2}};
        localStorageCart.editQuantity(newCartItem);

        let expectedResult = JSON.stringify([newCartItem]);

        expect(localStorage.getItem('cart')).toBe(expectedResult);
    });

    it('can edit correct product quantity when multiple products added', () => {
        localStorageCart.addCartItem(cartItem);
        localStorageCart.addCartItem(cartItem2);
        let newCartItem = {...cartItem, ...{quantity: 2}};
        localStorageCart.editQuantity(newCartItem);

        let expectedResult = JSON.stringify([newCartItem, cartItem2]);

        expect(localStorage.getItem('cart')).toBe(expectedResult);
    });
});
