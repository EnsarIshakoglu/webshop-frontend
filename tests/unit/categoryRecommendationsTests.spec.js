import * as productRecommendation from "../../src/services/localStorageProductRecommendation";
import LocalStorageMock from "../LocalStorageMock";

global.localStorage = new LocalStorageMock();

describe('Category storage', () => {

    beforeEach(() => {
        localStorage.clear();
    });

    it('adds category to localStorage', () => {
        productRecommendation.addVisitedCategory('Books');
        expect(productRecommendation.getVisitedCategories()).toEqual(['Books']);
    });

    it('uses first in first out principe', () => {
        productRecommendation.addVisitedCategory('Books');
        productRecommendation.addVisitedCategory('Movies');
        productRecommendation.addVisitedCategory('Games');
        productRecommendation.addVisitedCategory('Music');

        expect(productRecommendation.getVisitedCategories()).toEqual(['Music', 'Games', 'Movies']);
    });

    it('neglects categories that are already in the list', () => {
        productRecommendation.addVisitedCategory('Books');
        productRecommendation.addVisitedCategory('Books');
        productRecommendation.addVisitedCategory('Books');
        productRecommendation.addVisitedCategory('Music');

        expect(productRecommendation.getVisitedCategories()).toEqual(['Music', 'Books']);
    })
});
